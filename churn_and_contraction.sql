/*

Query for Churn & Contraction anaylsis that is visualized in Tableau
Solves for challenges with Reasons for Loss & Military Invasion
Should tie out to analysis also done by Finance

Tableau Dashboard: https://10az.online.tableau.com/#/site/gitlab/workbooks/1907429/views

*/

SELECT
    mo.CLOSE_DATE
    ,mo.CLOSE_MONTH
    ,mo.CLOSE_FISCAL_QUARTER_NAME
    ,mo.DIM_CRM_ACCOUNT_ID
    ,mo.CRM_ACCOUNT_NAME
    ,CASE WHEN mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_SALES_SEGMENT = 'Large' THEN '1. Large'
          WHEN mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_SALES_SEGMENT = 'PubSec' THEN '2. PubSec'
          WHEN mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_SALES_SEGMENT = 'Mid-Market' THEN '3. Mid-Market'
          WHEN mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_SALES_SEGMENT IN ('SMB','Unknown') THEN '4. SMB'
          ELSE 'SMB'
     END AS sales_segment
    ,mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_GEO AS parent_geo
    ,mo.PARENT_CRM_ACCOUNT_DEMOGRAPHICS_REGION AS parent_region
    ,mo.DIM_CRM_OPPORTUNITY_ID
    ,split_part(PARENT_CRM_ACCOUNT_DEMOGRAPHICS_TERRITORY,'_',1) AS territory_business_unit
    ,split_part(PARENT_CRM_ACCOUNT_DEMOGRAPHICS_TERRITORY,'_',2) AS territory_geo
    ,mo.OPPORTUNITY_NAME
    ,mo.ORDER_TYPE
    ,CASE WHEN mo.ORDER_TYPE IN ('4. Contraction','5. Churn - Partial','6. Churn - Final')
                   AND MILITARY_INVASION_RISK_SCALE__C IS NOT NULL THEN 'Military Invasion'
          WHEN mo.ORDER_TYPE IN ('4. Contraction','5. Churn - Partial') THEN 'Contraction'
          WHEN mo.ORDER_TYPE = '6. Churn - Final' THEN 'Churn'
     END AS loss_type
    ,CASE WHEN loss_type IS NOT NULL THEN nvl(mo.REASON_FOR_LOSS,mo.DOWNGRADE_REASON) END AS reason_for_loss_or_downgrade
    ,CASE WHEN loss_type = 'Military Invasion' THEN 'Military Invasion'
          WHEN reason_for_loss_or_downgrade = 'Corporate Decision' THEN 'Top Down Executive Decision'
          WHEN reason_for_loss_or_downgrade IN ('Lack of Engagement / Sponsor','Evangelist Left','Went Silent') THEN 'Lack of Customer Engagement or Sponsor'
          WHEN reason_for_loss_or_downgrade IN ('Budget/Value Unperceived','Product Value / Gaps') THEN 'Product Features / Value Gaps'
          WHEN reason_for_loss_or_downgrade IN ('Do Nothing','Other') THEN 'Unknown'
          WHEN reason_for_loss_or_downgrade IN ('Insuficient funds','Loss of Budget') THEN 'Lack of Budget'
          WHEN reason_for_loss_or_downgrade = 'Product quality/availability' THEN 'Product Quality / Availability'
          WHEN mo.ORDER_TYPE IN ('4. Contraction','5. Churn - Partial','6. Churn - Final') AND reason_for_loss_or_downgrade IS NULL THEN 'Unknown'
          ELSE reason_for_loss_or_downgrade
     END AS reason_for_loss_mapped_to_new_codes
    ,nvl(mo.REASON_FOR_LOSS_DETAILS,ro.DOWNGRADE_DETAILS__C) AS reason_for_loss_or_downgrade_details
    ,mo.COMPETITORS
    ,mo.NET_ARR
    ,mo.ARR_BASIS
    ,ro.ARR_BASIS_FOR_CLARI__C
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY mo
JOIN RAW.SALESFORCE_STITCH.OPPORTUNITY ro
    ON ro.ID = mo.DIM_CRM_OPPORTUNITY_ID
WHERE mo.FPA_MASTER_BOOKINGS_FLAG
    AND mo.GROWTH_TYPE NOT LIKE 'Missing%'
    AND iff(mo.NET_ARR = 0 AND mo.ARR_BASIS = 0,FALSE,TRUE) = TRUE
    AND mo.IS_NET_ARR_CLOSED_DEAL
--     AND mo.CLOSE_FISCAL_QUARTER_NAME IN ('FY23-Q1','FY23-Q2','FY23-Q3','FY23-Q4','FY24-Q1')
